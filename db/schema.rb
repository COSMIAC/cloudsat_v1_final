# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141123184205) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "adacs", force: true do |t|
    t.string   "adacs_name"
    t.integer  "satellite_ID"
    t.binary   "telemetry_adacs"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cdhs", force: true do |t|
    t.string   "cdh_name"
    t.integer  "satellite_ID"
    t.integer  "cdh_error_cnt"
    t.binary   "cdh_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ideas", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "picture"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payloads", force: true do |t|
    t.string   "payload_name"
    t.integer  "satellite_ID"
    t.binary   "telemetry_payload"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "power_systems", force: true do |t|
    t.string   "power_system_title"
    t.integer  "satellite_ID"
    t.decimal  "p_array1_i",         precision: 2, scale: 2
    t.decimal  "p_array1_t",         precision: 2, scale: 2
    t.decimal  "pn_array1_v",        precision: 2, scale: 2
    t.decimal  "n_array1_i",         precision: 2, scale: 2
    t.decimal  "n_array1_t",         precision: 2, scale: 2
    t.decimal  "pn_array2_v",        precision: 2, scale: 2
    t.decimal  "n_array2_i",         precision: 2, scale: 2
    t.decimal  "n_array2_t",         precision: 2, scale: 2
    t.decimal  "pn_array3_v",        precision: 2, scale: 2
    t.decimal  "p_array3_i",         precision: 2, scale: 2
    t.decimal  "p_array3_t",         precision: 2, scale: 2
    t.decimal  "p_array2_i",         precision: 2, scale: 2
    t.decimal  "p_array2_t",         precision: 2, scale: 2
    t.decimal  "n_array3_i",         precision: 2, scale: 2
    t.decimal  "n_array3_t",         precision: 2, scale: 2
    t.decimal  "p_array4_i",         precision: 2, scale: 2
    t.decimal  "p_array4_t",         precision: 2, scale: 2
    t.decimal  "pn_array4_v",        precision: 2, scale: 2
    t.decimal  "n_array4_i",         precision: 2, scale: 2
    t.decimal  "n_array4_t",         precision: 2, scale: 2
    t.decimal  "pn_array5_v",        precision: 2, scale: 2
    t.decimal  "n_array5_i",         precision: 2, scale: 2
    t.decimal  "n_array5_t",         precision: 2, scale: 2
    t.decimal  "pn_array6_v",        precision: 2, scale: 2
    t.decimal  "p_array6_i",         precision: 2, scale: 2
    t.decimal  "p_array6_t",         precision: 2, scale: 2
    t.decimal  "p_array5_i",         precision: 2, scale: 2
    t.decimal  "p_array5_t",         precision: 2, scale: 2
    t.decimal  "n_array6_i",         precision: 2, scale: 2
    t.decimal  "n_array6_t",         precision: 2, scale: 2
    t.decimal  "bus_3v3_i",          precision: 2, scale: 2
    t.decimal  "bus_5V_i",           precision: 2, scale: 2
    t.decimal  "bus_VBATT_i",        precision: 2, scale: 2
    t.decimal  "bus_12V_i",          precision: 2, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sat_comms", force: true do |t|
    t.string   "satcomm_name"
    t.integer  "satellite_ID"
    t.integer  "trans"
    t.integer  "receive"
    t.integer  "rf_error_count"
    t.decimal  "temp_RF",        precision: 2, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "satellites", force: true do |t|
    t.string   "satelline_name"
    t.string   "launch_date"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
