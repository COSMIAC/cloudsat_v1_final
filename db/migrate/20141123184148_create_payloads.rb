class CreatePayloads < ActiveRecord::Migration
  def change
    create_table :payloads do |t|

	  t.string :payload_name
	  t.integer :satellite_ID
	  t.binary :telemetry_payload
      t.timestamps
    end
  end
end
