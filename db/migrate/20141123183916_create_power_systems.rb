class CreatePowerSystems < ActiveRecord::Migration
  def change
    create_table :power_systems do |t|
	  
	  t.string :power_system_title
	  t.integer :satellite_ID	  
	  t.decimal :p_array1_i, precision: 2, scale: 2
	  t.decimal :p_array1_t, precision: 2, scale: 2
	  t.decimal :pn_array1_v, precision: 2, scale: 2
	  t.decimal :n_array1_i, precision: 2, scale: 2
	  t.decimal :n_array1_t, precision: 2, scale: 2
	  t.decimal :pn_array2_v, precision: 2, scale: 2
	  t.decimal :n_array2_i, precision: 2, scale: 2
	  t.decimal :n_array2_t, precision: 2, scale: 2
	  t.decimal :pn_array3_v, precision: 2, scale: 2
	  t.decimal :p_array3_i, precision: 2, scale: 2
	  t.decimal :p_array3_t, precision: 2, scale: 2
	  t.decimal :p_array2_i, precision: 2, scale: 2
	  t.decimal :p_array2_t, precision: 2, scale: 2
	  t.decimal :n_array3_i, precision: 2, scale: 2
	  t.decimal :n_array3_t, precision: 2, scale: 2
	  t.decimal :p_array4_i, precision: 2, scale: 2
	  t.decimal :p_array4_t, precision: 2, scale: 2
	  t.decimal :pn_array4_v, precision: 2, scale: 2
	  t.decimal :n_array4_i, precision: 2, scale: 2
	  t.decimal :n_array4_t, precision: 2, scale: 2
	  t.decimal :pn_array5_v, precision: 2, scale: 2
	  t.decimal :n_array5_i, precision: 2, scale: 2
	  t.decimal :n_array5_t, precision: 2, scale: 2
	  t.decimal :pn_array6_v, precision: 2, scale: 2
	  t.decimal :p_array6_i, precision: 2, scale: 2
	  t.decimal :p_array6_t, precision: 2, scale: 2
	  t.decimal :p_array5_i, precision: 2, scale: 2
	  t.decimal :p_array5_t, precision: 2, scale: 2
	  t.decimal :n_array6_i, precision: 2, scale: 2
	  t.decimal :n_array6_t, precision: 2, scale: 2
	  t.decimal :bus_3v3_i, precision: 2, scale: 2
	  t.decimal :bus_5V_i, precision: 2, scale: 2
	  t.decimal :bus_VBATT_i, precision: 2, scale: 2
	  t.decimal :bus_12V_i, precision: 2, scale: 2
	  
      t.timestamps
    end
  end
end
