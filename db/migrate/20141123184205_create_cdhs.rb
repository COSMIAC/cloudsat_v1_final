class CreateCdhs < ActiveRecord::Migration
  def change
    create_table :cdhs do |t|

	  t.string :cdh_name
	  t.integer :satellite_ID
	  t.integer :cdh_error_cnt
	  t.binary :cdh_status
      t.timestamps
    end
  end
end
