class CreateAdacs < ActiveRecord::Migration
  def change
    create_table :adacs do |t|

	  t.string :adacs_name
	  t.integer :satellite_ID
	  t.binary :telemetry_adacs
      t.timestamps
    end
  end
end
