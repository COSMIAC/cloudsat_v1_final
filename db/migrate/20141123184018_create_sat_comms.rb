class CreateSatComms < ActiveRecord::Migration
  def change
    create_table :sat_comms do |t|

	  t.string :satcomm_name
	  t.integer :satellite_ID
	  t.integer :trans
	  t.integer :receive
	  t.integer :rf_error_count
	  t.decimal :temp_RF, precision: 2, scale: 2
      t.timestamps
    end
  end
end
