class PayloadController < ApplicationController
  #def index
  #  render "index"
  #end 
  
  respond_to :html

  def index
    @payload1 = Payload.where('payload_name' => 'Payload 1')
    @payload2 = Payload.where('payload_name' => 'Payload 2')
    respond_with(@payload1,@payload2)
  end

end
