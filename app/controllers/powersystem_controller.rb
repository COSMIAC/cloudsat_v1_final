class PowersystemController < ApplicationController
  #before_action :set_powersystem, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @powersystems = PowerSystem.all
    respond_with(@powersystems)
  end
  
  #def index
  #  render "index"
  #end 
end
