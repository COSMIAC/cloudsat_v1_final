class SatcommController < ApplicationController
  
  respond_to :html

  def index
    @satcomm = SatComm.all
    @satcomm_last = SatComm.last
    respond_with(@satcomm_last, @satcomm)
  end
  
  #def index
  #  render "index"
  #end 
end
