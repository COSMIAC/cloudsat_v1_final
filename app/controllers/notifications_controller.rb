class NotificationsController < ApplicationController
  def index
  end

  def create
    Notifier.command_mail.deliver
    flash[:notice] = 'Command sent successfully.'
    redirect_to '/command'
  end
end
