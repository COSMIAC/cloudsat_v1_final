class Satellite < ActiveRecord::Base
	has_many :power_systems
	has_many :sat_comms
	has_many :adacs
	has_many :payloads
	has_many :cdhs
	
end
