class Notifier < ActionMailer::Base
  default from: "cloudsat@cosmiac.org"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.notifier.gmail_message.subject
  #
  def command_mail
    @greeting = "This is a test email"

    attachments['ORS_packet.bin'] = File.read('lib/ORS_packet.bin')

    mail to: "cloudsat@cosmiac.org"
  end
end