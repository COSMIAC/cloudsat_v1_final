Rails.application.routes.draw do
  #Routes for email system controllers
  get 'notifications/index'
  get 'notifications/create'
  get "/users/powersystem" => redirect("/powersystem")
  get "/users/satcomm" => redirect("/satcomm")
  get "/users/adacs" => redirect("/adacs")
  get "/users/cdh" => redirect("/cdh")
  get "/users/command" => redirect("/command")
  get "/users/payload" => redirect("/payload")
  
  #Routes for dashboard pages
  root :to => redirect('/powersystem')
  resources :adacs
  resources :cdh
  resources :powersystem
  resources :command
  resources :satcomm
  resources :payload
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
